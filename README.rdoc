PP-REST-RUBY
___________________________________________________________________________________________________
1. Install latest Ruby with version manager:
$ \curl -sSL https://get.rvm.io | bash -s stable --ruby

2. Install required gems (libraries):

$ gem install sinatra
$ gem install puma
___________________________________________________________________________________________________

Start server:
$ ruby server.rb -s Puma
___________________________________________________________________________________________________

Application address:
http://localhost:4567/
___________________________________________________________________________________________________

Example requests in cURL:

GET:
curl -i -H 'Application/json' -H 'Content-type: application/json' 'http://localhost:4567/users'

HEAD:
curl -I 'http://localhost:4567/users'

PUT:
curl -X PUT 'http://localhost:4567/users' -d 'id=987&name=bob&surname=blue'

POST:
curl -X POST 'http://localhost:4567/users' -d 'id=987&name=john&surname=red&birthDate=1991-05-10'

DELETE:
curl -X DELETE 'http://localhost:4567/users' -d 'id=987'

OPTIONS:
curl -v -X OPTIONS 'http://localhost:4567/users'

TRACE:

CONNECT:

PATCH:
curl -X PATCH 'http://localhost:4567/users' -d 'id=987&replace=name&value=tom'

___________________________________________________________________________________________________

You can use HTTPARTY gem (library) to send requests from irb console:
----------
Installation:
$ gem install httparty
----------
Require it:
require 'httparty'
----------
Commands:

GET:
HTTParty.get("http://localhost:4567/users/987").body

POST:
HTTParty.post("http://localhost:4567/users", body: { id: 987, name: 'Clint', surname: 'Printer', birthDate: '1955-12-12' }).body

PUT:
HTTParty.put("http://localhost:4567/users/987", body: { name: 'Chris', surname: 'White' }).body
___________________________________________________________________________________________________

Ruby doc: http://ruby-doc.org/core-2.3.1/
Sinatra doc: http://www.sinatrarb.com/documentation.html
