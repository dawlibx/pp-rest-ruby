# Requirments
require 'sinatra'
require 'sinatra/namespace'
require 'sinatra/base'
require './user.rb'
# require 'pry'
require 'ostruct'
require 'json'

# Endpoints
namespace '/users' do
  get '/:id' do
    user = User.find(params[:id])

    return unless user
    user.to_h.to_json
  end

  head '' do
  end

  put '/:id' do
    user = User.put_update(params)

    return unless user
    { 'result': 'OK' }.to_json
  end

  post '' do
    user = User.add(params)

    return unless user
    { 'result': 'OK' }.to_json
  end

  delete '/:id' do
    user = User.delete(params[:id])

    return unless user
    { 'result': 'OK' }.to_json
  end

  options '' do
    response.headers['Allow'] = 'GET, HEAD, PUT, POST, DELETE, OPTIONS, TRACE, CONNECT, PATCH'

    nil
  end

  patch '/:id' do
    user = User.patch_update(params)

    return unless user
    { 'result': 'OK' }.to_json
  end
end
