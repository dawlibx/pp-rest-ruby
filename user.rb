# Class representing User
class User < OpenStruct
  @@all = []

  class << self
    def add(params)
      new_user = new(params)

      @@all << new_user

      new_user
    end

    def all
      @@all
    end

    def find(id)
      @@all.find { |user| user.id == id }
    end

    def put_update(params)
      user = find(params[:id])

      return unless user

      params.each do |key, value|
        user[key] = value
      end

      user
    end

    def patch_update(params)
      user = find(params[:id])

      return unless user

      user[params[:replace]] = params[:value]

      user
    end

    def delete(id)
      user = find(id)

      @@all.delete(user)
    end
  end
end
